import { createStore } from 'vuex'
import createHttp from "@/services/http";
import router from "@/router";

const store = createStore({
  state: {
    colors: [],
    token: "",
    // expiration: Date.now(),
    isBusy: false,
    error: ""
  },
  mutations: {
    setColors: (state, colors) => state.colors = colors,
    setBusy: (state) => state.isBusy = true,
    clearBusy: (state) => state.isBusy = false,
    setError: (state, error) => state.error = error,
    clearError: (state) => state.error = "",
    setToken: (state, token) => {
      state.token = token;
      // state.expiration = new Date(model.expiration)

    },
    clearToken: (state) => {
      state.token = "";
      state.expiration = Date.now();
      state.colors = [];
    }
  },
  getters: {
    isAuthenticated: (state) => state.token.length > 0,
    getToken: (state) => state.token
  },
  actions: {
    loadColors: async ({ commit }) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(); // secured
        // const colors = await http.get("http://localhost:8081/restricted");
        const colors = await http.get("http://localhost:8081/colors");
        commit("setColors", colors.data);
      } catch (err) {
        commit("setError", "Failed getting colors. Try logging in...");
      } finally {
        commit("clearBusy");
      }
    },
    login: async ({ commit }, model) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(false); // unsecured
        const modelAsJson = JSON.stringify(model);
        await http.post("http://localhost:8081/login", modelAsJson)
            .then( result => {
              const token = result.headers["authorization"];
              commit("setToken", token);
              router.push("/");
            })
            .catch( err => {
              console.log("error i post");
              console.log(err);
            });
      } catch (err){
        console.log("CATCH!");
        console.log(err);
        commit("setError", "Failed to login");
      } finally {
        commit("clearBusy");
      }
    },
    logout: ({ commit }) => {
      commit("clearToken");
      router.push("/");
    }
  }
})

export default store

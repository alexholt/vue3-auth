import axios from "axios";
import store  from "@/store/index";


export default function createHttp(secured = true) {

    if (secured) {
        let ting = axios.create({
            headers: {
                "Authorization":  store.state.token
            }
        });
        return ting;
    } else {
        let ting2 = axios.create();

        return ting2;
    }
}
